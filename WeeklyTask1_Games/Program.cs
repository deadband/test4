﻿using Ninject;

namespace WeeklyTask1_Games
{
    internal static class Program
    {
        private static void Main()
        {
            var kernel = new StandardKernel(new DiModule());
            var gameExecutor = kernel.Get<GameExecutor>();

            while (true)
            {
                gameExecutor.Play();
            }
        }
    }
}