﻿using System;
using Newtonsoft.Json;
using System.Linq;
using WeeklyTask1_Games.DataAccess;
using WeeklyTask1_Games.DataAccess.Entites;
using System.IO;

namespace WeeklyTask1_Games
{
    internal class GameExecutor
    {
        private readonly IGame _game;
        private readonly IScoresRepository _scoresRepository;

        public delegate void GameFinisherEventHandler(object sender, EventArgs args);
        public event GameFinisherEventHandler Gamefinished;


        public GameExecutor(IGame game, IScoresRepository scoresRepository)
        {
            _game = game;
            _scoresRepository = scoresRepository;
        }

        public void Play()
        {
            Console.WriteLine("You play " + _game.Name + " game!");

            var gameScore = new GameScore
            {
                PlayerName = GetPlayerName(),
                Score = PerformGame()
            };

            
            Console.WriteLine("Congratulations " + gameScore.PlayerName + "! You scored " + gameScore.Score + " points");

            DisplayPlaceOnList(gameScore);
            ExportToJson();

        _scoresRepository.Add(gameScore);

        }

        private void ExportToJson()
        {
            Console.WriteLine("Do you want to generate JSON file with all scores? [Y/N]");
            var choice = Console.ReadLine();
            if (choice.ToLower() == "y")
            {
                var listToSeriallize = _scoresRepository.GetAll();
                var serializedObject = new JsonSerializer();
                var path = "scores.json";
                using (var file = new StreamWriter(path))
                {
                    serializedObject.Serialize(file, listToSeriallize);
                }
            }
            else return;
        }

        private void DisplayPlaceOnList(GameScore gameScore)
        {
            var highScores = _scoresRepository.GetAll().OrderBy(s => s.Score).ToList();
            var placeOnHighScoreList = 0;
            for (int i = 0; i < highScores.Count(); i++)
            {
                if (gameScore.Score == highScores[i].Score)
                {
                    placeOnHighScoreList = i + 1;
                    break;
                }
            }
            Console.WriteLine($"{gameScore.PlayerName} scored {gameScore.Score}, place on hihgscores list {placeOnHighScoreList}" );
        }

        private int PerformGame()
        {
            _game.Play();
            return _game.Score;
        }

        private string GetPlayerName()
        {
            Console.Write("Enter your nickname: ");
            return Console.ReadLine();
        }
    }
}