﻿using System;

namespace WeeklyTask1_Games
{
    internal class GuessGame : IGame
    {
        public string Name => "Guess Game";

        public int Score { get; private set; }
        private int _numberToGuess;

        public void Play()
        {
            ResetScore();
            _numberToGuess = new Random(DateTime.Now.Millisecond).Next(0, 1000);
            GameLoop();
        }

        private void GameLoop()
        {
            while (true)
            {
                Console.WriteLine("Guess the number: ");
                var guessedNumber = ReadInt();

                if (guessedNumber == _numberToGuess)
                {
                    break;
                }
                Score++;

                if (guessedNumber > _numberToGuess)
                {
                    Console.WriteLine("Too much");
                    continue;
                }

                Console.WriteLine("Too little");
            }
        }

        private int ReadInt()
        {
            int number;

            while (true)
            {
                try
                {
                    number = int.Parse(Console.ReadLine() ?? throw new InvalidOperationException());
                    break;
                }
                catch (Exception)
                {
                    Console.WriteLine("Try again...");
                }
            }
            return number;
        }

        public void ResetScore()
        {
            Score = 0;
        }
    }
}