﻿using System.Collections.Generic;
using WeeklyTask1_Games.DataAccess.Entites;

namespace WeeklyTask1_Games.DataAccess
{
    public interface IScoresRepository
    {
        void Add(GameScore score);
        IList<GameScore> GetAll();
    }
}