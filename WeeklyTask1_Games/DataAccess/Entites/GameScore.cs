﻿namespace WeeklyTask1_Games.DataAccess.Entites
{
    public class GameScore
    {
        public int Id { get; set; }
        public string PlayerName { get; set; }
        public int Score { get; set; }
    }
}