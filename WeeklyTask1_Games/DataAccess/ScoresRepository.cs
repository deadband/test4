﻿using System.Collections.Generic;
using System.Linq;
using WeeklyTask1_Games.DataAccess.Entites;

namespace WeeklyTask1_Games.DataAccess
{
    public class ScoresRepository : IScoresRepository
    {
        public void Add(GameScore score)
        {
            using (var dbContext = new ScoresDbContext())
            {
                dbContext.GameScores.Add(score);
                dbContext.SaveChanges();
            }
        }

        public IList<GameScore> GetAll()
        {
            using (var dbContext = new ScoresDbContext())
            {
                return dbContext.GameScores.ToList();
            }
        }
    }
}