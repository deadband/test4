﻿namespace WeeklyTask1_Games
{
    internal interface IGame
    {
        string Name { get; }
        int Score { get; }

        void Play();
        void ResetScore();
    }
}