﻿using Ninject.Modules;
using WeeklyTask1_Games.DataAccess;
namespace WeeklyTask1_Games
{
    class DiModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IGame>().To<GuessGame>();
            Bind<IScoresRepository>().To<ScoresRepository>();
            Bind<ScoresDbContext>().To<ScoresDbContext>().InSingletonScope();
        }
    }
}